/**
	* This functionality is for use with the 1kpl.us sharing bookmarklets for RSS Feed Readers
	*	and was inspired by the original Google Reader Sharing functionality before Google 
	*	went stupid and ruined it.
	* This functionality is instantiated via Bookmarklet in browsers, with the following bookmarklet:
	*	javascript:(function()%7BShareURL=encodeURIComponent(location.href);ShareTitle=encodeURIComponent(document.title);share1kplus_script=document.createElement('SCRIPT');share1kplus_script.type='text/javascript';share1kplus_script.src='http://joshuarhoades.com/1kplus/1kplus-share.js?x='+(Math.random());document.getElementsByTagName('head')%5B0%5D.appendChild(share1kplus_script);%7D)();
	* @author Josh Rhoades (http://joshuarhoades.com/)
	* @version 0.0.2
	* @LastUpdated 10.4.12
	
*/
/* namespace specific to 1kpl.us bookmarklet */
var ns1kplus = ns1kplus || {}; ns1kplus.utils = ns1kplus.utils || {}; ns1kplus.utils.debug = ns1kplus.utils.debug || {}; ns1kplus.refs = ns1kplus.refs || {}; ns1kplus.app = ns1kplus.app || {};
ns1kplus = {
	
};
ns1kplus.refs = {
	pathInfo: (function() {
		var pi = {};
			pi.baseDomain     = 'http://www.joshuarhoades.com',
			pi.baseRoot       = '/1kplus/',
			pi.baseURL        = pi.baseDomain + pi.baseRoot,
			pi.frameURL       = pi.baseURL + 'bookmarklet.html',
			pi.frameName      = '1kplus_bookmarklet_iframe',
			pi.stylesheet	= '1kplus-share-mini.css',
			pi.shareContainer = '1kplus_bookmarklet';
		return pi;
	})(),
	metaInfo: (function() {
		var mi = {};
			mi.appName = '1kpl.us bookmarklet',
			mi.version       = 'v0.0.2',
			mi.lastUpdate    = '10.15.2012',
			mi.stringShare   = '',
			mi.parentDomain  = document.domain,
			mi.debugEnabled  = true;
		return mi;
	})(),
	debugInfo: (function() {
		var di = {};
			di.devMode       = true,
			di.splitter = '###################################################################',
			di.logTypes = ['Info','Warn','Error'],
			di.msgIntro      = 'Debug Mode',
			di.noMessage = 'No log message was set, cannot log debug statement!',
			di.cflr = "\n",
			di.msgDisabled   = 'Production Mode, log disabled';
/**
	* Function will determine if in debug mode based on presence of "debug=true" in queryString, otherwise default to FALSE (non-debug mode)
	* @Author josh rhoades
	* @version 0.0.1
	* @TODO add functionality that will set this to TRUE if in a non-Production environment, despite QS value. NameSpace needs to be built out for this
	* @throw {Boolean} true|false based on presence of "debug=true" in QueryString
	* @see ns1kplus.utils.getQueryString('debug')
*/
			di.enabled = function() {
				if ((ns1kplus.utils.getQueryString('debug') && ns1kplus.utils.getQueryString('debug').toLowerCase() === 'true') || (ns1kplus.refs.debugInfo['devMode'])) {
					//debug query string is set to true, set BLS.utils.debug['enabled'] to return TRUE only if override is set to false
					//TODO: add functionality that will set this to TRUE if in a non-Production environment, despite QS value. NameSpace needs to be built out for this
					return true;
				} else {
					//debug mode is not set in QueryString and override is not set TRUE, set ns1kplus.utils.debug['enabled'] to return FALSE to signify debug mode is off
					return false;
				}
			}
		return di;
	})(),
/*
	baseDomain: 'http://www.joshuarhoades.com/',
	baseRoot: '/1kplus/',
	baseURL: ns1kplus.refs['baseDomain'] + ns1kplus.refs['baseRoot'],
	frameURL: ns1kplus.refs['baseURL'] + 'bookmarklet.html',
	stylesheet: '1kplus-share-mini.css',
	frameName: '1kplus_bookmarklet_iframe',
	shareContainer: '1kplus_bookmarklet',
	parentDomain: document.domain,
	stringShare: ''
*/
/*
	currentLocation: document.location.href,
	currentLocationEnd: (ns1kplus.refs['currentLocation'].indexOf("?") == -1) ? ns1kplus.refs['currentLocation'].length : ns1kplus.refs['currentLocation'].indexOf("?"),
	currentFilename: ns1kplus.refs['currentLocation'].substring(ns1kplus.refs['currentLocation'].lastIndexOf("/")+1, ns1kplus.refs['currentLocationEnd'])
*/
	
};
ns1kplus.utils = {
/**
	* This function will qet all queryString elements available by name (key)
	* @Author: Josh Rhoades
	* @param {String} key - queryString parameter to match on
	* @param {String} default_ - 
	* @throw {String} 
*/
	getQueryString: function(strKey) {
		strKey = strKey.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		var rxPattern = new RegExp("[\\?&]" + strKey + "=([^&#]*)");
		var arrQS = rxPattern.exec(window.location.href);
		if (arrQS == null) {
			return false;
		} else {
			return arrQS[1];
		}
	},
/**
	* Function will take in a DOM Object, find its parent, and remove the object from its parent,
	*	thereby destroying the object. Checks to make sure the child and parent are not the same object
	* @author Josh Rhoades
	* @param {DOM Element} elObject - DOM Element/Object to target for destruction
*/
	destroyElement: function(elObject) {
		if ((elObject && elObject.parentNode) && (elObject != elObject.parentNode)) {
			elObject.parentNode.removeChild(elObject);
			ns1kplus.utils.doLogging('Removing child element ' + elObject + ' from parentNode ' + elObject.parentNode);
		}
	},
/**
	* Function is used as a debug helper to consolidate all logging statements,
	*	while allowing for ns1kplus.debug to be set to FALSE and disable all logging
	*	statements for Production mode
	* @author Josh Rhoades
	* @param {String} strLog - string of text to log
	* @throws {console.log()} - logged of debug prefix text with passed in text to console.log()
*/
	doLogging: function(strLog, intType) {
		if (ns1kplus.refs.debugInfo.enabled()) {
			intType = intType || 0;
			strLog = (strLog) ? strLog : ns1kplus.refs.debugInfo['noMessage'];
			strOutput = ns1kplus.refs.debugInfo['splitter'] + strLog + ns1kplus.refs.debugInfo['cflr'];
			strOutput += ns1kplus.refs.debugInfo['msgIntro'] + ' - ' + ns1kplus.refs.metaInfo['appName'] + ' ' + ns1kplus.refs.metaInfo['version'] + ', Last Updated: ' + ns1kplus.refs.metaInfo['lastUpdate'] + ns1kplus.refs.debugInfo['cflr'];
			strOutput += '@ ' + new Date().toString();
			strOutput += ns1kplus.refs.debugInfo['logTypes'][intType] + ': ' + strLog + ns1kplus.refs.debugInfo['cflr'];
			strOutput += ns1kplus.refs.debugInfo['splitter'] + strLog + ns1kplus.refs.debugInfo['cflr'];
			console.log(strOutput);
		}
	}
};
/**
	* Function will take a parent element and a newly created child element, and append the child to the parent.
	*	Expects to be passed objects
	* @Author Josh Rhoades
	* @Throws DOM event to appendChild of target to parent target
	* @param {DOMObject} elParent - parent DOM element
	* @param {DOMObject} elChild - child DOM element to be appended to parent element
*/
ns1kplus.addChild = function(elChild, elParent) {
	if (elParent && elChild) {
		elParent.appendChild(elChild);
		ns1kplus.utils.doLogging('Appending ' + elChild.id + ' to ' + elParent.id);
	} else {
		ns1kplus.utils.doLogging('AppendChild Error: either parent or child or both were not provided');
	}
}
/* globals */
ns1kplus.utils.doLogging('filename = ' + ns1kplus.currentFilename);
ns1kplus.utils.doLogging('qs = ' +  ns1kplus.utils.getQueryString('ShareTitle'));
ns1kplus.utils.doLogging('qs = ' +  ns1kplus.utils.getQueryString('ShareURL'));
/*
var strShareTitle = unescape(ns1kplus.utils.getQueryString('ShareTitle')),
	strShareURL = unescape(ns1kplus.utils.getQueryString('ShareURL')),
	strContent = '';
*/
/*
var shareBaseURL = 'http://www.joshuarhoades.com/1kplus/';
var shareFramePath = shareBaseURL + 'bookmarklet.html';
var shareCSS = '1kplus-share-mini.css';
var shareiFrameName = '1kplus_bookmarklet_iframe';
var shareContainer = '1kplus_bookmarklet';
var shareDomain = document.domain;
*/
//ns1kplus.utils.doLogging('title = "' + unescape(ShareTitle) + '", url= "' + unescape(ShareURL) + '"');
/*
function getEmbed() {
	var e = window.frames['1kplus_bookmarklet_iframe'];
	return e;
}
*/

function fixURLs(objContent) {
	var childNodes = objContent.childNodes;
	var allLinks = objContent.querySelectorAll('a');
	var allImg = objContent.querySelectorAll('img');
	for (var i=0; i < allLinks.length; i++) {
                var redTag = allLinks[i];
                ns1kplus.utils.doLogging('The contents of the ' + (i + 1) + ' . red element is' + "\n" + redTag.href);
            }
            
	ns1kplus.utils.doLogging('t length = ' + childNodes.length);
	for (i=0; i<childNodes.length; i++) {
		ns1kplus.utils.doLogging('tag name = ' + childNodes[i].tagName);
		if (childNodes[i].links) {
			//has a link
			ns1kplus.utils.doLogging('href value = ' + childNodes[i].href.value);
		}
	}
	
}

function addCSS(strPath){
	var headID = document.getElementsByTagName('head')[0], cssNode = document.createElement('link');
	cssNode.type = 'text/css';
	cssNode.rel = 'stylesheet';
	cssNode.href = ns1kplus.refs.pathInfo['baseURL'] + strPath;
	ns1kplus.utils.doLogging('Injecting CSS file: ' + strPath);
	cssNode.media = 'screen';
	headID.appendChild(cssNode);
}
/**
	* Function will listen for keypresses of the ESC key and toggle the visibility of the share container.
	*	Conditional checks for different key events xbrowsers
	* @Author Josh Rhoades
	* @Throws logging event, toggleItem function call
	* @param {event} e - keypress event
*/
function keyPressHandler(e) {
	//set vars based on MSIE or other browser key events
	var kbdKC = (window.event) ? event.keyCode : e.keyCode, kbdESC = (window.event) ? 27 : e.DOM_VK_ESCAPE;
	ns1kplus.utils.doLogging('kbdKC = ' + kbdKC + ', kbdESC = ' + kbdESC);
	if(kbdKC == kbdESC) {
		ns1kplus.utils.doLogging('Esc pressed');
		toggleItem(ns1kplus.shareContainer);
	}
}
/**
	* Fucntion will take an ID passed in and retrieve that element. Once the DOM element is retrieved, it will
	*	grab its innerHTML as source and return it back to the parent call.
	* @Author Josh Rhoades
	* @param {String} strTarget - string ID of DOM element to retrieve
	* @return {String} objTarget.innerHTML - innerHTML of targeted div from strTarget param
*/
function getElementSource(strTarget) {
	var strReturn = '';
	if (strTarget) {
		objTarget = document.getElementById(strTarget);
		if (objTarget) {
			return objTarget.innerHTML;
		} else {
			ns1kplus.utils.doLogging('Could not find target DOM element with ID of ' + strTarget);
			return false;
		}
	} else {
		ns1kplus.utils.doLogging('Could not get source of DOM element as no target was passed to getElementSource()');
		return false;
	}
}

(function() {
	addCSS(ns1kplus.refs.pathInfo['stylesheet']);//add 1kplus CSS dynamically
	// get the currently selected text
	var objShareSource;
	try {
		ns1kplus.stringShare = ((window.getSelection && window.getSelection()) || (document.getSelection && document.getSelection()) || (document.selection && document.selection.createRange && document.selection.createRange().htmlText));
/* 		alert('first ns1kplus.stringShare = ' + ns1kplus.stringShare); */
		if (ns1kplus.stringShare && window.getSelection()) {
			var a = window.getSelection();
			if (0 < a.rangeCount) {
				ns1kplus.stringShare = a.getRangeAt(0).cloneContents();
/* 				alert('second ns1kplus.stringShare = ' + ns1kplus.stringShare.toString()); */
			}
		}
	} catch(e) {
		// access denied on https sites
		ns1kplus.utils.doLogging('Access denied on https sites');
		ns1kplus.stringShare = '';
	}
/*
	if (ns1kplus.stringShare == '') {
		ns1kplus.stringShare = '';
	}
*/
/*
	var iframe_url = ns1kplus.frameURL + '?shareURL=' + ShareURL + '&shareTitle=' + ShareTitle + '&shareString=' + ns1kplus.stringShare;
	var existing_iframe = document.getElementById(ns1kplus.frameName);
	if (existing_iframe) {
		// if has text selected, copy into iframe
		if (ns1kplus.stringShare != "") {
			existing_iframe.src = iframe_url;
		} else {
			ns1kplus.utils.doLogging('permission denied!');
			// want to set focus back to that item! but can't; access denied
		}
		return;
	}
*/
	/* share content div */
	var div = document.createElement('div');
	div.className = 'share1kplus';
	div.style.position = 'fixed';
	div.style.background = "#FFFFFF", div.style.border = "4px solid #c3d9ff", div.style.top = "8px", div.style.right = "8px", div.style.width = "550px", div.style.height = "378px", div.style.zIndex = 2147483647;
	div.id = ns1kplus.shareContainer;
	div.innerHTML = '<p><input type="text" id="shareTitle" class="shareTitle" value="' + unescape(ShareTitle) + '" title="Click to edit the Share Title"/></p>' + "\n" +
			'<p class="small"><label for="shareURL">Source:</label><br/>' + "\n" + 
			'<input type="text" id="shareURL" class="small shareURL" value="' + unescape(ShareURL) + '" title="Click to edit the Share URL" /></p>';
	//create div that will old shared HTML elements chosen by user
	divShareDiv = document.createElement('div');
	divShareDiv.id = 'shareContent';
	divShareDiv.contentEditable = true;
	divShareDiv.style = 'border: 1px solid #000000; overflow: auto; height: 250px;';
	divShareDiv.onclick = function() {
		//toggle to textarea to edit
	}
	divShareDiv.onblur = function() {
		//toggle back to DIV to edit
	}
	divShareDiv.appendChild(ns1kplus.stringShare);
	ns1kplus.addChild(divShareDiv, div);

	var divContent = document.createElement('div');
	divContent.className = 'shareContent-1kplus';
	ns1kplus.addChild(div, divContent);/* div.appendChild(divContent); */
/* 	ns1kplus.addChild(divContent, ns1kplus.stringShare);// divContent.appendChild(t); */
	/* /share content div */

/* 	create share iframe */
	var elIframe = document.createElement('iframe');
	elIframe.frameBorder = 0;
	elIframe.scrolling = 'no';
	elIframe.name = ns1kplus.frameName;
	elIframe.src = ns1kplus.frameURL;
	elIframe.width = 550;
	elIframe.style = 'background-color: #FFFFFF;'
	//div.appendChild(elIframe);/* div.appendChild(elIframe); */
	
/* 	action bar - close/save */
	var divActions = document.createElement('div');
	divActions.className = '1kplus-share-actions';
	divActions.id = '1kplus-share-actions';
	divActions.style = 'clear:both;'
	ns1kplus.addChild(div, divActions);/* div.appendChild(divActions); */
	
	
/* 	close button */
	var elCloseBtn = document.createElement('input');
	elCloseBtn.className = 'btnClose-1kplus';
	elCloseBtn.id = 'btnClose-1kplus';
	elCloseBtn.type = 'button';
	elCloseBtn.value = 'Close me hard';
	elCloseBtn.onclick = function() {
		ns1kplus.destroyElement(div);
	}
	ns1kplus.addChild(divActions, elCloseBtn);/* divActions.appendChild(elCloseBtn); */
	
/* 	save button */
	var elSaveBtn = document.createElement('input');
	elSaveBtn.className = 'btnSave-1kplus';
	elSaveBtn.id = 'btnSave-1kplus';
	elSaveBtn.type = 'button';
	elSaveBtn.value = 'Share!';
	elSaveBtn.onclick = function() {

	}
	ns1kplus.addChild(divActions, elSaveBtn);/* divActions.appendChild(elSaveBtn); */
/*
	var str = '';
	str += '<p>Content:</p>';
	str += share1kplusString;
	str += '<hr/>';
	str += '<iframe frameborder="0" scrolling="no" name="' + ns1kplus.frameName + '" id="' + ns1kplus.frameName + '" src="' + iframe_url + '" width="550px" style="backgroundColor: #FFFFFF;"></iframe>';
*/
	//div.innerHTML = str;
	//document.getElementById(ns1kplus.frameName).shareContent.innerHTML = share1kplusString;

	//turn on the sharing frame
	document.body.insertBefore(div, document.body.firstChild);
	//document.getElementById(ns1kplus.frameName).contentWindow.updateShare(ShareURL,ShareTitle,share1kplusString);
/*
		objShareTarget = document.getElementById('shareContent');
	objShareTarget.innerHTML = ns1kplus.stringShare;
*/
})()