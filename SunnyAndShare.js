/*
SonnyAndShare.js - 1kpl.us Sharing Bookmarklet
Inspired by the formerly awesome Google Reader
(c) 2011 - all rights reserved
*/
(function () {
	var h = void 0,
	i = !0,
	j = null,
	k = !1,
	p, aa = "/1kplus/share-frame.html";
	var q = this;
	function ba() {}
	function s(a) {
		a.s = function () {
			return a.ja || (a.ja = new a)
		}
	}
	function t(a) {
		var b = typeof a;
		if ("object" == b) if (a) {
			if (a instanceof Array) return "array";
			if (a instanceof Object) return b;
			var c = Object.prototype.toString.call(a);
			if ("[object Window]" == c) return "object";
			if ("[object Array]" == c || "number" == typeof a.length && "undefined" != typeof a.splice && "undefined" != typeof a.propertyIsEnumerable && !a.propertyIsEnumerable("splice")) return "array";
			if ("[object Function]" == c || "undefined" != typeof a.call && "undefined" != typeof a.propertyIsEnumerable && !a.propertyIsEnumerable("call")) return "function"
		} else return "null";
		else if ("function" == b && "undefined" == typeof a.call) return "object";
		return b
	}
	function u(a) {
		return "string" == typeof a
	}
	function v(a) {
		return "function" == t(a)
	}
	function w(a) {
		return a[ca] || (a[ca] = ++da)
	}
	var ca = "closure_uid_" + Math.floor(2147483648 * Math.random()).toString(36),
	da = 0;
	function ea(a, b, c) {
		return a.call.apply(a.R, arguments)
	}
	function fa(a, b, c) {
		if (!a) throw Error();
		if (2 < arguments.length) {
			var d = Array.prototype.slice.call(arguments, 2);
			return function () {
				var e = Array.prototype.slice.call(arguments);
				Array.prototype.unshift.apply(e, d);
				return a.apply(b, e)
			}
		}
		return function () {
			return a.apply(b, arguments)
		}
	}
	function ga(a, b, c) {
		ga = Function.prototype.R && -1 != Function.prototype.R.toString().indexOf("native code") ? ea: fa;
		return ga.apply(j, arguments)
	}
	function ha(a, b) {
		var c = Array.prototype.slice.call(arguments, 1);
		return function () {
			var d = Array.prototype.slice.call(arguments);
			d.unshift.apply(d, c);
			return a.apply(this, d)
		}
	}
	function x(a, b) {
		function c() {}
		c.prototype = b.prototype;
		a.d = b.prototype;
		a.prototype = new c;
		a.prototype.constructor = a
	}
	function ia(a, b) {
		for (var c = 1; c < arguments.length; c++) var d = ("" + arguments[c]).replace(/\$/g, "$$$$"),
		a = a.replace(/\%s/, d);
		return a
	}
	function ja(a) {
		return a.replace(/^[\s\xa0]+|[\s\xa0]+$/g, "")
	}
	var ka = /^[a-zA-Z0-9\-_.!~*'()]*$/;
	function la(a) {
		a = "" + a;
		return ! ka.test(a) ? encodeURIComponent(a) : a
	}
	function y(a, b) {
		if (b) return a.replace(ma, "&amp;").replace(na, "&lt;").replace(oa, "&gt;").replace(pa, "&quot;");
		if (!qa.test(a)) return a; - 1 != a.indexOf("&") && (a = a.replace(ma, "&amp;")); - 1 != a.indexOf("<") && (a = a.replace(na, "&lt;")); - 1 != a.indexOf(">") && (a = a.replace(oa, "&gt;")); - 1 != a.indexOf('"') && (a = a.replace(pa, "&quot;"));
		return a
	}
	var ma = /&/g,
	na = /</g,
	oa = />/g,
	pa = /\"/g,
	qa = /[&<>\"]/;
	function ra(a, b) {
		for (var c = 0, d = ja("" + a).split("."), e = ja("" + b).split("."), f = Math.max(d.length, e.length), g = 0; 0 == c && g < f; g++) {
			var l = d[g] || "",
			m = e[g] || "",
			n = RegExp("(\\d*)(\\D*)", "g"),
			H = RegExp("(\\d*)(\\D*)", "g");
			do {
				var o = n.exec(l) || ["", "", ""], r = H.exec(m) || ["", "", ""];
				if (0 == o[0].length && 0 == r[0].length) break;
				c = ((0 == o[1].length ? 0 : parseInt(o[1], 10)) < (0 == r[1].length ? 0 : parseInt(r[1], 10)) ? -1 : (0 == o[1].length ? 0 : parseInt(o[1], 10)) > (0 == r[1].length ? 0 : parseInt(r[1], 10)) ? 1 : 0) || ((0 == o[2].length) < (0 == r[2].length) ? -1 : (0 == o[2].length) > (0 == r[2].length) ? 1 : 0) || (o[2] < r[2] ? -1 : o[2] > r[2] ? 1 : 0)
			} while (0 == c)
		}
		return c
	}
	var A = Array.prototype,
	sa = A.indexOf ?
	function (a, b, c) {
		return A.indexOf.call(a, b, c)
	}: function (a, b, c) {
		c = c == j ? 0 : 0 > c ? Math.max(0, a.length + c) : c;
		if (u(a)) return ! u(b) || 1 != b.length ? -1 : a.indexOf(b, c);
		for (; c < a.length; c++) if (c in a && a[c] === b) return c;
		return - 1
	},
	ta = A.forEach ?
	function (a, b, c) {
		A.forEach.call(a, b, c)
	}: function (a, b, c) {
		for (var d = a.length, e = u(a) ? a.split("") : a, f = 0; f < d; f++) f in e && b.call(c, e[f], f, a)
	},
	ua = A.every ?
	function (a, b, c) {
		return A.every.call(a, b, c)
	}: function (a, b, c) {
		for (var d = a.length, e = u(a) ? a.split("") : a, f = 0; f < d; f++) if (f in e && !b.call(c, e[f], f, a)) return k;
		return i
	};
	function B(a, b) {
		return 0 <= sa(a, b)
	}
	function va(a, b) {
		var c = sa(a, b),
		d;
		(d = 0 <= c) && A.splice.call(a, c, 1);
		return d
	}
	function wa(a, b, c, d) {
		return A.splice.apply(a, xa(arguments, 1))
	}
	function xa(a, b, c) {
		return 2 >= arguments.length ? A.slice.call(a, b) : A.slice.call(a, b, c)
	}
	var ya;
	function za(a) {
		return (a = a.className) && "function" == typeof a.split ? a.split(/\s+/) : []
	}
	function Aa(a, b) {
		var c = za(a),
		d = xa(arguments, 1),
		e;
		e = c;
		for (var f = 0, g = 0; g < d.length; g++) B(e, d[g]) || (e.push(d[g]), f++);
		e = f == d.length;
		a.className = c.join(" ");
		return e
	}
	function Ba(a, b) {
		var c = za(a),
		d = xa(arguments, 1),
		e;
		e = c;
		for (var f = 0, g = 0; g < e.length; g++) B(d, e[g]) && (wa(e, g--, 1), f++);
		e = f == d.length;
		a.className = c.join(" ");
		return e
	}
	function Ca(a, b, c) {
		for (var d in a) b.call(c, a[d], d, a)
	}
	var Da = "constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",");
	function Ea(a, b) {
		for (var c, d, e = 1; e < arguments.length; e++) {
			d = arguments[e];
			for (c in d) a[c] = d[c];
			for (var f = 0; f < Da.length; f++) c = Da[f],
			Object.prototype.hasOwnProperty.call(d, c) && (a[c] = d[c])
		}
	}
	var C, Fa, Ga, Ha;
	function Ia() {
		return q.navigator ? q.navigator.userAgent: j
	}
	function Ja() {
		return q.navigator
	}
	Ha = Ga = Fa = C = k;
	var Ka;
	if (Ka = Ia()) {
		var La = Ja();
		C = 0 == Ka.indexOf("Opera");
		Fa = !C && -1 != Ka.indexOf("MSIE");
		Ga = !C && -1 != Ka.indexOf("WebKit");
		Ha = !C && !Ga && "Gecko" == La.product
	}
	var Ma = C,
	D = Fa,
	Na = Ha,
	E = Ga;
	Ja();
	Ja() && Ja();
	var Oa;
	a: {
		var Pa = "",
		F;
		if (Ma && q.opera) var Qa = q.opera.version,
		Pa = "function" == typeof Qa ? Qa() : Qa;
		else if (Na ? F = /rv\:([^\);]+)(\)|;)/: D ? F = /MSIE\s+([^\);]+)(\)|;)/: E && (F = /WebKit\/(\S+)/), F) var Ra = F.exec(Ia()),
		Pa = Ra ? Ra[1] : "";
		if (D) {
			var Sa, Ta = q.document;
			Sa = Ta ? Ta.documentMode: h;
			if (Sa > parseFloat(Pa)) {
				Oa = "" + Sa;
				break a
			}
		}
		Oa = Pa
	}
	var Ua = Oa,
	Va = {};
	function G(a) {
		return Va[a] || (Va[a] = 0 <= ra(Ua, a))
	}
	var Wa = {};
	function Xa(a) {
		return Wa[a] || (Wa[a] = D && document.documentMode && document.documentMode >= a)
	}
	function Ya(a) {
		Ya[" "](a);
		return a
	}
	Ya[" "] = ba; ! D || Xa(9);
	var Za = !D || Xa(9);
	D && G("8");
	function I() {}
	I.prototype.K = k;
	I.prototype.h = function () {
		this.K || (this.K = i, this.a())
	};
	I.prototype.a = function () {
		this.ga && $a.apply(j, this.ga)
	};
	function $a(a) {
		for (var b = 0, c = arguments.length; b < c; ++b) {
			var d = arguments[b],
			e = d,
			f = t(e);
			"array" == f || "object" == f && "number" == typeof e.length ? $a.apply(j, d) : d && "function" == typeof d.h && d.h()
		}
	}
	function J(a, b) {
		this.type = a;
		this.currentTarget = this.target = b
	}
	x(J, I);
	J.prototype.a = function () {
		delete this.type;
		delete this.target;
		delete this.currentTarget
	};
	J.prototype.k = k;
	J.prototype.H = i;
	function K(a, b) {
		a && this.i(a, b)
	}
	x(K, J);
	p = K.prototype;
	p.target = j;
	p.relatedTarget = j;
	p.offsetX = 0;
	p.offsetY = 0;
	p.clientX = 0;
	p.clientY = 0;
	p.screenX = 0;
	p.screenY = 0;
	p.button = 0;
	p.keyCode = 0;
	p.charCode = 0;
	p.ctrlKey = k;
	p.altKey = k;
	p.shiftKey = k;
	p.metaKey = k;
	p.i = function (a, b) {
		var c = this.type = a.type;
		J.call(this, c);
		this.target = a.target || a.srcElement;
		this.currentTarget = b;
		var d = a.relatedTarget;
		if (d) {
			if (Na) {
				var e;
				a: {
					try {
						Ya(d.nodeName);
						e = i;
						break a
					} catch(f) {}
					e = k
				}
				e || (d = j)
			}
		} else "mouseover" == c ? d = a.fromElement: "mouseout" == c && (d = a.toElement);
		this.relatedTarget = d;
		this.offsetX = a.offsetX !== h ? a.offsetX: a.layerX;
		this.offsetY = a.offsetY !== h ? a.offsetY: a.layerY;
		this.clientX = a.clientX !== h ? a.clientX: a.pageX;
		this.clientY = a.clientY !== h ? a.clientY: a.pageY;
		this.screenX = a.screenX || 0;
		this.screenY = a.screenY || 0;
		this.button = a.button;
		this.keyCode = a.keyCode || 0;
		this.charCode = a.charCode || ("keypress" == c ? a.keyCode: 0);
		this.ctrlKey = a.ctrlKey;
		this.altKey = a.altKey;
		this.shiftKey = a.shiftKey;
		this.metaKey = a.metaKey;
		this.state = a.state;
		delete this.H;
		delete this.k
	};
	p.a = function () {
		K.d.a.call(this);
		this.relatedTarget = this.currentTarget = this.target = j
	};
	function ab() {}
	var bb = 0;
	p = ab.prototype;
	p.key = 0;
	p.l = k;
	p.S = k;
	p.i = function (a, b, c, d, e, f) {
		if (v(a)) this.Y = i;
		else if (a && a.handleEvent && v(a.handleEvent)) this.Y = k;
		else throw Error("Invalid listener argument");
		this.t = a;
		this.$ = b;
		this.src = c;
		this.type = d;
		this.v = !!e;
		this.M = f;
		this.S = k;
		this.key = ++bb;
		this.l = k
	};
	p.handleEvent = function (a) {
		return this.Y ? this.t.call(this.M || this.src, a) : this.t.handleEvent.call(this.t, a)
	};
	var L = {},
	M = {},
	N = {},
	O = {};
	function cb(a, b, c, d, e) {
		if (b) {
			if ("array" == t(b)) {
				for (var f = 0; f < b.length; f++) cb(a, b[f], c, d, e);
				return j
			}
			var d = !!d,
			g = M;
			b in g || (g[b] = {
				e: 0,
				c: 0
			});
			g = g[b];
			d in g || (g[d] = {
				e: 0,
				c: 0
			},
			g.e++);
			var g = g[d],
			l = w(a),
			m;
			g.c++;
			if (g[l]) {
				m = g[l];
				for (f = 0; f < m.length; f++) if (g = m[f], g.t == c && g.M == e) {
					if (g.l) break;
					return m[f].key
				}
			} else m = g[l] = [],
			g.e++;
			f = db();
			f.src = a;
			g = new ab;
			g.i(c, f, a, b, d, e);
			c = g.key;
			f.key = c;
			m.push(g);
			L[c] = g;
			N[l] || (N[l] = []);
			N[l].push(g);
			a.addEventListener ? (a == q || !a.U) && a.addEventListener(b, f, d) : a.attachEvent(b in O ? O[b] : O[b] = "on" + b, f);
			return c
		}
		throw Error("Invalid event type");
	}
	function db() {
		var a = eb,
		b = Za ?
		function (c) {
			return a.call(b.src, b.key, c)
		}: function (c) {
			c = a.call(b.src, b.key, c);
			if (!c) return c
		};
		return b
	}
	function fb(a, b, c, d, e) {
		if ("array" == t(b)) {
			for (var f = 0; f < b.length; f++) fb(a, b[f], c, d, e);
			return j
		}
		d = !!d;
		a: {
			f = M;
			if (b in f && (f = f[b], d in f && (f = f[d], a = w(a), f[a]))) {
				a = f[a];
				break a
			}
			a = j
		}
		if (!a) return k;
		for (f = 0; f < a.length; f++) if (a[f].t == c && a[f].v == d && a[f].M == e) return gb(a[f].key);
		return k
	}
	function gb(a) {
		if (!L[a]) return k;
		var b = L[a];
		if (b.l) return k;
		var c = b.src,
		d = b.type,
		e = b.$,
		f = b.v;
		c.removeEventListener ? (c == q || !c.U) && c.removeEventListener(d, e, f) : c.detachEvent && c.detachEvent(d in O ? O[d] : O[d] = "on" + d, e);
		c = w(c);
		e = M[d][f][c];
		if (N[c]) {
			var g = N[c];
			va(g, b);
			0 == g.length && delete N[c]
		}
		b.l = i;
		e.Z = i;
		hb(d, f, c, e);
		delete L[a];
		return i
	}
	function hb(a, b, c, d) {
		if (!d.D && d.Z) {
			for (var e = 0, f = 0; e < d.length; e++) d[e].l ? d[e].$.src = j: (e != f && (d[f] = d[e]), f++);
			d.length = f;
			d.Z = k;
			0 == f && (delete M[a][b][c], M[a][b].e--, 0 == M[a][b].e && (delete M[a][b], M[a].e--), 0 == M[a].e && delete M[a])
		}
	}
	function ib(a, b, c) {
		var d = 0,
		e = a == j,
		f = b == j,
		g = c == j,
		c = !!c;
		if (e) Ca(N, function (m) {
			for (var n = m.length - 1; 0 <= n; n--) {
				var H = m[n];
				if ((f || b == H.type) && (g || c == H.v)) gb(H.key),
				d++
			}
		});
		else if (a = w(a), N[a]) {
			a = N[a];
			for (e = a.length - 1; 0 <= e; e--) {
				var l = a[e];
				if ((f || b == l.type) && (g || c == l.v)) gb(l.key),
				d++
			}
		}
		return d
	}
	function P(a, b, c, d, e) {
		var f = 1,
		b = w(b);
		if (a[b]) {
			a.c--;
			a = a[b];
			a.D ? a.D++:a.D = 1;
			try {
				for (var g = a.length, l = 0; l < g; l++) {
					var m = a[l];
					m && !m.l && (f &= jb(m, e) !== k)
				}
			} finally {
				a.D--,
				hb(c, d, b, a)
			}
		}
		return Boolean(f)
	}
	function jb(a, b) {
		var c = a.handleEvent(b);
		a.S && gb(a.key);
		return c
	}
	function eb(a, b) {
		if (!L[a]) return i;
		var c = L[a],
		d = c.type,
		e = M;
		if (! (d in e)) return i;
		var e = e[d],
		f,
		g;
		if (!Za) {
			var l;
			if (! (l = b)) a: {
				l = "window.event".split(".");
				for (var m = q; f = l.shift();) if (m[f] != j) m = m[f];
				else {
					l = j;
					break a
				}
				l = m
			}
			f = l;
			l = i in e;
			m = k in e;
			if (l) {
				if (0 > f.keyCode || f.returnValue != h) return i;
				a: {
					var n = k;
					if (0 == f.keyCode) try {
						f.keyCode = -1;
						break a
					} catch(H) {
						n = i
					}
					if (n || f.returnValue == h) f.returnValue = i
				}
			}
			n = new K;
			n.i(f, this);
			f = i;
			try {
				if (l) {
					for (var o = [], r = n.currentTarget; r; r = r.parentNode) o.push(r);
					g = e[i];
					g.c = g.e;
					for (var z = o.length - 1; ! n.k && 0 <= z && g.c; z--) n.currentTarget = o[z],
					f &= P(g, o[z], d, i, n);
					if (m) {
						g = e[k];
						g.c = g.e;
						for (z = 0; ! n.k && z < o.length && g.c; z++) n.currentTarget = o[z],
						f &= P(g, o[z], d, k, n)
					}
				} else f = jb(c, n)
			} finally {
				o && (o.length = 0),
				n.h()
			}
			return f
		}
		d = new K(b, this);
		try {
			f = jb(c, d)
		} finally {
			d.h()
		}
		return f
	}
	var kb = 0,
	lb = RegExp("^(?:([^:/?#.]+):)?(?://(?:([^/?#]*)@)?([\\w\\d\\-\\u0100-\\uffff.%]*)(?::([0-9]+))?)?([^?#]+)?(?:\\?([^#]*))?(?:#(.*))?$");
	function mb(a) {
		return a && decodeURIComponent(a)
	}
	function nb(a) {
		if (a[1]) {
			var b = a[0],
			c = b.indexOf("#");
			0 <= c && (a.push(b.substr(c)), a[0] = b = b.substr(0, c));
			c = b.indexOf("?");
			0 > c ? a[1] = "?": c == b.length - 1 && (a[1] = h)
		}
		return a.join("")
	}
	var ob = /#|$/;
	function pb(a, b) {
		var c = a.search(ob),
		d;
		a: {
			d = 0;
			for (var e = b.length; 0 <= (d = a.indexOf(b, d)) && d < c;) {
				var f = a.charCodeAt(d - 1);
				if (38 == f || 63 == f) if (f = a.charCodeAt(d + e), !f || 61 == f || 38 == f || 35 == f) break a;
				d += e + 1
			}
			d = -1
		}
		if (0 > d) return j;
		e = a.indexOf("&", d);
		if (0 > e || e > c) e = c;
		d += b.length + 1;
		return decodeURIComponent(a.substr(d, e - d).replace(/\+/g, " "))
	} ! D || Xa(9); ! Na && !D || D && Xa(9) || Na && G("1.9.1");
	D && G("9");
	function Q(a) {
		return u(a) ? document.getElementById(a) : a
	}
	function qb(a) {
		this.V = a || q.document || document
	}
	qb.prototype.j = function (a) {
		return u(a) ? this.V.getElementById(a) : a
	};
	qb.prototype.createElement = function (a) {
		return this.V.createElement(a)
	};
	qb.prototype.appendChild = function (a, b) {
		a.appendChild(b)
	};
	function rb() {}
	x(rb, I);
	p = rb.prototype;
	p.U = i;
	p.F = j;
	p.P = function (a) {
		this.F = a
	};
	p.addEventListener = function (a, b, c, d) {
		cb(this, a, b, c, d)
	};
	p.removeEventListener = function (a, b, c, d) {
		fb(this, a, b, c, d)
	};
	p.dispatchEvent = function (a) {
		var b = a.type || a,
		c = M;
		if (b in c) {
			if (u(a)) a = new J(a, this);
			else if (a instanceof J) a.target = a.target || this;
			else {
				var d = a,
				a = new J(b, this);
				Ea(a, d)
			}
			var d = 1,
			e, c = c[b],
			b = i in c,
			f;
			if (b) {
				e = [];
				for (f = this; f; f = f.F) e.push(f);
				f = c[i];
				f.c = f.e;
				for (var g = e.length - 1; ! a.k && 0 <= g && f.c; g--) a.currentTarget = e[g],
				d &= P(f, e[g], a.type, i, a) && a.H != k
			}
			if (k in c) if (f = c[k], f.c = f.e, b) for (g = 0; ! a.k && g < e.length && f.c; g++) a.currentTarget = e[g],
			d &= P(f, e[g], a.type, k, a) && a.H != k;
			else for (e = this; ! a.k && e && f.c; e = e.F) a.currentTarget = e,
			d &= P(f, e, a.type, k, a) && a.H != k;
			a = Boolean(d)
		} else a = i;
		return a
	};
	p.a = function () {
		rb.d.a.call(this);
		ib(this);
		this.F = j
	};
	function sb() {}
	s(sb);
	sb.prototype.la = 0;
	sb.s();
	function R(a) {
		a || ya || (ya = new qb)
	}
	x(R, rb);
	p = R.prototype;
	p.ia = sb.s();
	function tb(a, b) {
		switch (a) {
		case 1:
			return b ? "disable": "enable";
		case 2:
			return b ? "highlight": "unhighlight";
		case 4:
			return b ? "activate": "deactivate";
		case 8:
			return b ? "select": "unselect";
		case 16:
			return b ? "check": "uncheck";
		case 32:
			return b ? "focus": "blur";
		case 64:
			return b ? "open": "close"
		}
		throw Error("Invalid component state");
	}
	p.X = j;
	p.N = k;
	p.b = j;
	p.G = j;
	p.w = j;
	p.p = j;
	p.pa = k;
	p.getId = function () {
		return this.X || (this.X = ":" + (this.ia.la++).toString(36))
	};
	p.j = function () {
		return this.b
	};
	p.P = function (a) {
		if (this.G && this.G != a) throw Error("Method not supported");
		R.d.P.call(this, a)
	};
	p.r = function () {
		ub(this, function (a) {
			a.N && a.r()
		});
		this.B && this.B.qa();
		this.N = k
	};
	p.a = function () {
		R.d.a.call(this);
		this.N && this.r();
		this.B && (this.B.h(), delete this.B);
		ub(this, function (a) {
			a.h()
		}); ! this.pa && this.b && this.b && this.b.parentNode && this.b.parentNode.removeChild(this.b);
		this.G = this.b = this.p = this.w = j
	};
	function ub(a, b, c) {
		a.w && ta(a.w, b, c)
	}
	p.removeChild = function (a, b) {
		if (a) {
			var c = u(a) ? a: a.getId(),
			a = this.p && c ? (c in this.p ? this.p[c] : h) || j: j;
			if (c && a) {
				var d = this.p;
				c in d && delete d[c];
				va(this.w, a);
				b && (a.r(), a.b && a.b && a.b.parentNode && a.b.parentNode.removeChild(a.b));
				c = a;
				if (c == j) throw Error("Unable to set parent component");
				c.G = j;
				R.d.P.call(c, j)
			}
		}
		if (!a) throw Error("Child is not in parent component");
		return a
	};
	kb++;
	function S() {}
	var vb;
	s(S);
	p = S.prototype;
	p.A = function (a, b, c) {
		if (a = a.j ? a.j() : a) if (D && !G("7")) {
			var d = wb(this, za(a), b);
			d.push(b);
			ha(c ? Aa: Ba, a).apply(j, d)
		} else c ? Aa(a, b) : Ba(a, b)
	};
	p.aa = function (a, b) {
		var c;
		if (a.u & 32 && (c = a.L())) {
			if (!b && a.g & 32) {
				try {
					c.blur()
				} catch(d) {}
				a.g & 32 && (a.Q & 4 && a.u & 4 && a.setActive(k), a.Q & 32 && a.u & 32 && xb(a, 32, k) && a.o(32, k))
			}
			var e;
			(e = c.getAttributeNode("tabindex")) && e.specified ? (e = c.tabIndex, e = "number" == typeof e && 0 <= e && 32768 > e) : e = k;
			e != b && (b ? c.tabIndex = 0 : (c.tabIndex = -1, c.removeAttribute("tabIndex")))
		}
	};
	p.o = function (a, b, c) {
		var d = a.j();
		if (d) {
			var e;
			this.T || (e = this.W(), this.T = {
				1 : e + "-disabled",
				2 : e + "-hover",
				4 : e + "-active",
				8 : e + "-selected",
				16 : e + "-checked",
				32 : e + "-focused",
				64 : e + "-open"
			});
			(e = this.T[b]) && this.A(a, e, c);
			this.J(d, b, c)
		}
	};
	p.J = function (a, b, c) {
		vb || (vb = {
			1 : "disabled",
			8 : "selected",
			16 : "checked",
			64 : "expanded"
		});
		(b = vb[b]) && a.setAttribute("aria-" + b, c)
	};
	p.L = function (a) {
		return a.j()
	};
	p.W = function () {
		return "goog-control"
	};
	function wb(a, b, c) {
		var d = [];
		c && (b = b.concat([c]));
		ta([], function (e) {
			ua(e, ha(B, b)) && (!c || B(e, c)) && d.push(e.join("_"))
		});
		return d
	}
	function T() {}
	x(T, S);
	s(T);
	T.prototype.J = function (a, b, c) {
		16 == b ? a.setAttribute("aria-pressed", c) : T.d.J.call(this, a, b, c)
	};
	T.prototype.W = function () {
		return "goog-button"
	};
	D || E && G("525");
	function yb(a, b) {
		if (!a) throw Error("Invalid class name " + a);
		if (!v(b)) throw Error("Invalid decorator function " + b);
	}
	var zb = {};
	function U(a, b, c) {
		R.call(this, c);
		if (! (a = b)) {
			for (var a = this.constructor, d; a;) {
				d = w(a);
				if (d = zb[d]) break;
				a = a.d ? a.d.constructor: j
			}
			a = d ? v(d.s) ? d.s() : new d: j
		}
		this.m = a
	}
	x(U, R);
	p = U.prototype;
	p.g = 0;
	p.u = 39;
	p.Q = 255;
	p.na = 0;
	p.oa = i;
	p.f = j;
	p.L = function () {
		return this.m.L(this)
	};
	p.A = function (a, b) {
		b ? a && (this.f ? B(this.f, a) || this.f.push(a) : this.f = [a], this.m.A(this, a, i)) : a && this.f && (va(this.f, a), 0 == this.f.length && (this.f = j), this.m.A(this, a, k))
	};
	p.r = function () {
		U.d.r.call(this);
		this.C && this.C.detach();
		this.oa && !(this.g & 1) && this.m.aa(this, k)
	};
	p.a = function () {
		U.d.a.call(this);
		this.C && (this.C.h(), delete this.C);
		delete this.m;
		this.f = j
	};
	p.setActive = function (a) {
		xb(this, 4, a) && this.o(4, a)
	};
	p.o = function (a, b) {
		this.u & a && b != !!(this.g & a) && (this.m.o(this, a, b), this.g = b ? this.g | a: this.g & ~a)
	};
	function xb(a, b, c) {
		return !! (a.u & b) && !!(a.g & b) != c && (!(a.na & b) || a.dispatchEvent(tb(b, c))) && !a.K
	}
	if (!v(U)) throw Error("Invalid component class " + U);
	if (!v(S)) throw Error("Invalid renderer class " + S);
	var Ab = w(U);
	zb[Ab] = S;
	yb("goog-control", function () {
		return new U(j)
	});
	function V() {}
	x(V, T);
	s(V);
	V.prototype.aa = ba;
	V.prototype.o = function (a, b, c) {
		V.d.o.call(this, a, b, c);
		if ((a = a.j()) && 1 == b) a.disabled = c
	};
	V.prototype.J = ba;
	function Bb(a, b, c) {
		U.call(this, a, b || V.s(), c)
	}
	x(Bb, U);
	Bb.prototype.a = function () {
		Bb.d.a.call(this);
		delete this.sa;
		delete this.ra
	};
	yb("goog-button", function () {
		return new Bb(j)
	});
	D && G(8);
	"ScriptEngine" in q && "JScript" == q.ScriptEngine() && (q.ScriptEngineMajorVersion(), q.ScriptEngineMinorVersion(), q.ScriptEngineBuildVersion());
	if (window._LOGIN_URL === h) {
		var W;
		window._IS_MULTILOGIN_ENABLED ? (W = Cb("AddSession", i), W += "&Email=" + encodeURIComponent(_USER_EMAIL)) : W = Cb("ServiceLogin", i);
		_LOGIN_URL = W
	}
	var Db = {};
	function Cb(a, b) {
		var c = "https://www.google.com/accounts/" + a + "?service=reader&passive=true&nui=1&ltmpl=default";
		if (b) {
			var d;
			try {
				d = window.top.location.href
			} catch(e) {
				d = window.location.href
			}
			c += "&continue=" + encodeURIComponent(d) + "&followup=" + encodeURIComponent(d)
		}
		return c
	}
	function Eb() {
		var a = {};
		if (!window || !window.location || !window.location.href) return a;
		var b = window.location.href.split("#")[1];
		if (!b) return a;
		for (var b = b.split("&"), c = 0, d; d = b[c]; c++) {
			var e = d.indexOf("=");
			a[d.substring(0, e)] = d.substring(e + 1)
		}
		return a
	}
	function Fb(a, b) {
		try {
			a.location.replace(b)
		} catch(c) {
			a.location = b
		}
	}
	function Gb() {
		window.opener ? window.close() : top != window && Hb(top) ? window.top.removeLinkFrame() : Fb(top, _OPENER_URL.split("#")[0] + "#close=1")
	}
	function _FR_LinkBookmarkletUtil_initCloseLink() {
		cb(Q("close-link"), "click", Gb)
	}
	function Hb(a) {
		try {
			return a._USER_ID !== h && a._USER_EMAIL !== h
		} catch(b) {
			return k
		}
	}
	function _finishSignIn() {
		_LINK_BOOKMARKLET_IS_STANDALONE ? window.location.reload(i) : Fb(top, _OPENER_URL.split("#")[0] + "#refresh=1")
	}
	function X() {}
	X.prototype.z = function () {
		return j
	};
	function Ib() {}
	x(Ib, X);
	function Jb() {
		if (document.selection && document.selection.createRange) {
			if (document.selection.createRange().text) return document.selection.createRange().htmlText
		} else if (window.getSelection) {
			var a = window.getSelection();
			if (0 < a.rangeCount) {
				var b = document.createElement("div");
				b.appendChild(a.getRangeAt(0).cloneContents());
				return b.innerHTML
			}
		}
		return ""
	}
	Ib.prototype.z = function () {
		return Jb(this)
	};
	function Kb() {}
	x(Kb, X);
	Kb.prototype.z = function () {
		for (var a = document.getElementsByTagName("meta"), b = 0, c; c = a[b]; b++) {
			var d = c.getAttribute("name");
			if (d && "DESCRIPTION" == d.toUpperCase()) return c.getAttribute("content")
		}
		return j
	};
	function Lb() {}
	x(Lb, X);
	Lb.prototype.z = function () {
		var a = mb(window.location.href.match(lb)[3] || j);
		if (a) var b = a.length - 12,
		a = 0 <= b && a.indexOf(".youtube.com", b) == b || "youtube.com" == a;
		else a = k;
		if (!a || "/watch" != window.location.pathname) return j;
		a = document.getElementById("embed_code");
		if (!a) {
			var c = pb(window.location.search, "v");
			c ? (c = ia("http://www.youtube.com/v/%s&fs=1", c), c = ['<object align="middle" width="400" height="326"><param name="allowScriptAccess" value="never"><param name="allowFullScreen" value="true"><param name="wmode" value="transparent"><param name="movie" value="', c, '"><embed width="400px" height="326px" type="application/x-shockwave-flash" src="', c, '" allowScriptAccess="never" allowFullScreen="true" quality="best" bgcolor="#ffffff" wmode="transparent" FlashVars="playerMode=embedded" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed></object>'].join("")) : c = j;
			return c
		}
		if (/^[\s\xa0]*$/.test(a.value == j ? "": "" + a.value) ? 0 : 0 == a.value.toLowerCase().lastIndexOf("<object", 0) || 0 == a.value.toLowerCase().lastIndexOf("<embed", 0)) return a.value;
		var a = window.location.href,
		b = mb(a.match(lb)[5] || j),
		d = b.match("/v/([^&?]+)[&?]?.*$");
		d && 1 < d.length ? c = d[1] : 0 == b.lastIndexOf("/watch", 0) && (c = pb(a, "v"), c || (c = a.indexOf("#"), (c = mb(0 > c ? j: a.substr(c + 1))) && 0 == c.lastIndexOf("!", 0) && (c = c.substr(1)), c = pb("?" + c, "v")));
		return (c = c ? ia("http://i.ytimg.com/vi/%s/default.jpg", c) : "") ? '<a href="' + window.location + '"><img src="' + c + '"></a><br><a href="' + window.location + '">' + window.location + "</a>": j
	};
	var Mb = [new Lb, new Ib, new Kb, new X];
	function Nb(a, b, c) {
		a[b] && (Fb(window, window.location.href.split("#")[0] + "#"), c())
	}
	function Ob() {
		var a = (window.CoreReader________bookmarklet_domain || window.location.protocol + "//" + window.location.host) + aa;
		"at" in Db || (Db.at = pb(window.location.search, "at"));
		var b = Db.at || window.CoreReader________AT;
		b && (a = nb([a, "&", "at", "=", la(b)]));
		return a
	}
	function Pb() {
		var a = new Qb;
		a.da = document.title;
		a.ea = window.location.href;
		var b;
		a: {
			var c = j;
			for (b = 0; c = Mb[b]; b++) if (c = c.z()) {
				b = c;
				break a
			}
			b = ""
		}
		b = y(b);
		a.I = b;
		a.ba = window.location.host;
		a.ca = window.location.protocol + "//" + window.location.host + "/";
		return a
	}
	function Rb(a, b) {
		if (!document) return k;
		var c = document.contentType,
		d;
		if (d = c) c ? (0 == c.lastIndexOf("x-", 0) && (c = c.substring(2)), c = ja(c.split("/")[0]).toLowerCase()) : c = "",
		d = c == a;
		return d || document.body && 2 == document.body.childNodes.length && document.body.firstChild.tagName && document.body.firstChild.tagName.toLowerCase() == b ? i: k
	}
	var Sb = j;
	function Tb(a, b) {
		this.O = a;
		this.fa = b
	}
	Tb.prototype.clear = function (a) {
		window.clearInterval(this.ka);
		if (!this.O) {
			var b = Q("CoreReader________link_bookmarklet_node");
			b.innerHTML = "";
			a && b.parentNode.removeChild(b);
			Sb = j
		}
	};
	function Ub() {
		var a;
		a = E ? window.frames.CoreReader________link_bookmarklet_frame: (a = Q("CoreReader________link_bookmarklet_frame")) ? a.contentWindow: j;
		return a ? i: k
	}
	Tb.prototype.ha = function () {
		if (this.O || Ub(this)) {
			var a = Eb(),
			b = this;
			Nb(a, "refresh", function () {
				b.clear();
				b.i(Pb())
			});
			Nb(a, "close", ga(b.clear, b, i))
		}
	};
	Tb.prototype.i = function (a) {
		this.O ? window.open(this.fa ? Vb(a, Ob()) : "", "CoreReader________link_bookmarklet_frame", "height=378,width=520") ? this.fa || Wb(this, a) : alert("Grrr! A popup blocker may be preventing you from sharing this page! If you have a popup blocker, try disabling it to open the window.") : Ub(this) || (Q("CoreReader________link_bookmarklet_node").innerHTML = '<iframe frameborder="0" id="CoreReader________link_bookmarklet_frame" name="CoreReader________link_bookmarklet_frame" style="width:100%;height:100%;border:0px;padding:0px;margin:0px"></iframe>', Wb(this, a));
		this.ka = window.setInterval(ga(this.ha, this), 50)
	};
	function Wb(a, b) {
		var c = Xb(b, Ob(), "CoreReader________link_bookmarklet_frame");
		document.body.appendChild(c);
		c.submit()
	}
	function Qb() {}
	function Yb(a) {
		var b = document.location.pathname.split("/");
		a.da = b[b.length - 1];
		a.ea = document.location.href;
		a.ba = window.location.host;
		a.ca = window.location.protocol + "//" + window.location.host + "/"
	}
	function Zb(a, b) {
		if (a.ma) b("srcItemId", a.ma);
		else {
			b("title", a.da);
			b("url", a.ea);
			b("srcTitle", a.ba);
			b("srcUrl", a.ca);
			var c = a.I;
			1E5 < c.length && (c = c.substring(0, 99997) + "...");
			b("snippet", c)
		}
	}
	function Xb(a, b, c) {
		var d = document.createElement("form");
		d.method = "POST";
		d.target = c;
		d.action = b;
		d.acceptCharset = "utf-8";
		var e = [];
		Zb(a, function (f, g) {
			g && e.push('<input type="hidden" name="' + y(f) + '" value="' + y(g) + '">')
		});
		d.innerHTML = e.join("");
		return d
	}
	function Vb(a, b) {
		var c = b;
		Zb(a, function (d, e) {
			e && (c = nb([c, "&", d, "=", la(e)]))
		});
		return c
	}
	function $b(a, b, c) {
		b = b || D || Ma;
		if (!b) {
			document.body.scrollTop = document.documentElement.scrollTop = 0;
			var d = Q("CoreReader________link_bookmarklet_node");
			d || (d = document.createElement("div"), d.id = "CoreReader________link_bookmarklet_node", d.style.position = D && 0 == ra(Ua, "6") ? "absolute": "fixed", d.style.background = "#fff", d.style.border = "4px solid #c3d9ff", d.style.top = "8px", d.style.right = "8px", d.style.width = "520px", d.style.height = "378px", d.style.zIndex = 2147483647, document.body.appendChild(d))
		}
		Sb = new Tb(b, c);
		Sb.i(a)
	}
	function ac() {
		Sb.clear(i)
	}
	var Y = "removeLinkFrame".split("."),
	Z = q; ! (Y[0] in Z) && Z.execScript && Z.execScript("var " + Y[0]);
	for (var $; Y.length && ($ = Y.shift());) ! Y.length && ac !== h ? Z[$] = ac: Z = Z[$] ? Z[$] : Z[$] = {};
	if (!Hb(window)) if (Rb("video", "embed")) {
		var bc = new Qb;
		Yb(bc);
		var cc = '<a href="' + y(document.location.href) + '">Video</a>';
		bc.I = cc;
		$b(bc, i, i)
	} else if (Rb("image", "img")) {
		var dc = new Qb;
		Yb(dc);
		var ec = '<img src="' + y(document.location.href) + '">';
		dc.I = ec;
		$b(dc, E ? k: i, E ? k: i)
	} else if (Rb("", "pre")) {
		var fc = new Qb;
		Yb(fc);
		var gc = "<pre>" + y(document.getElementsByTagName("pre")[0].innerHTML) + "</pre>";
		fc.I = gc;
		$b(fc, i, i)
	} else $b(Pb());
})();
/*
bookmarklet syntax
javascript:var%20b=document.body;var%20CoreReader________bookmarklet_domain='http://www.joshuarhoades.com';jsURL=encodeURIComponent(location.href);jsTitle=encodeURIComponent(document.title);if(b&&!document.xmlVersion){void(z=document.createElement('script'));void(z.src='http://www.joshuarhoades.com/1kplus/SunnyAndShare.js');void(b.appendChild(z));}else{}
*/